provider "aws" {
  region = var.aws_reg
  profile = "default"
}